import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget{
  @override
  _HelloFutterAppState createState() => _HelloFutterAppState();
}
String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";
String thaiGreeting = "สวัสดี Flutter";
String koreanGreeting = "안녕하세요 Flutter";

class _HelloFutterAppState extends State<HelloFlutterApp> {
  String displayText = englishGreeting;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      //Scaffold Widget
      home: Scaffold(
        appBar: AppBar(
          title: Text("Hello Flutter"),
          leading: Icon(Icons.home),
          actions: <Widget>[
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == englishGreeting?
                    spanishGreeting:englishGreeting;
                  });
                },
                icon: Icon(Icons.refresh)),
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == englishGreeting?
                    thaiGreeting:englishGreeting;
                  });
                },
                icon: Icon(Icons.face)),
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == englishGreeting?
                    koreanGreeting:englishGreeting;
                  });
                },
                icon: Icon(Icons.favorite)),
          ],
        ),
        body: Center(
          child: Text(
              displayText,
              style: TextStyle(fontSize: 24)),
        ),
      ),
    );
  }
}
